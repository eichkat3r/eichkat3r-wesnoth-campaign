# wesnoth-addons

Each addon lives in its own subdirectory.
You may take a look at [[https://wiki.wesnoth.org/Create]] to get an insight on how to create addons for wesnoth.